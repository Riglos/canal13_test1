import React, { useState, useEffect } from "react";
import { FlatList, View, StyleSheet, ActivityIndicator } from "react-native";
import axios from "axios";
import Item from "../components/Item";
import Loading from "../components/Loading";
import { URL } from "../constants/theme";

export default function ListScreen() {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [error, setError] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  const functionmakeRemoteRequest = () => {
    // const url = `http://5e16456b22b5c600140cf9bf.mockapi.io/api/v1/test?limit=10&page=${page}`;
    const url = `${URL} + ${page}`;
    return axios
      .get(url)
      .then(res => {
        data.length != 0 ? setData(data.concat(res.data)) : setData(res.data);
        page == 10 ? setPage(1) : setPage(page + 1);
        setRefreshing(false);
      })
      .catch(err => {
        setError(err.message);
        setRefreshing(false);
      });
  };

  useEffect(() => {
    functionmakeRemoteRequest();
  }, []);

  handleLoadMore = () => {
    setRefreshing(true);
    functionmakeRemoteRequest();
  };

  renderFooter = () => {
    return (
      <View>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {data.length != 0 ? (
        <FlatList
          data={data}
          renderItem={({ item }) => <Item item={item} />}
          keyExtractor={item => item.number.toString()}
          refreshing={refreshing}
          // onRefresh={() => handleRefresh()}
          onEndReachedThreshold={0.5}
          onEndReached={this.handleLoadMore}
          ListFooterComponent={this.renderFooter}
          // onEndReached={() => handleRefresh()}
        />
      ) : (
        <Loading />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  }
});

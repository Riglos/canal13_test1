import React from "react";
import { View, StyleSheet, Text } from "react-native";

import Correct from "./Correct";

export default function Item({ item }) {
  return (
    <View key={item.number} style={styles.item}>
      <View style={styles.block}>
        <View style={styles.card}>
          <View style={styles.number}>
            <Text style={styles.insideNumber}>{item.number}</Text>
          </View>
          <Text style={styles.question}>{item.question}</Text>
          <View style={styles.answerContainer}>
            <Correct item={item} />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  item: {
    padding: 20
  },
  block: {
    borderWidth: 2,
    elevation: 2,
    borderColor: "rgba(46,91,255,0.08)",
    shadowColor: "rgba(46,91,255,0.07)",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: { width: 0, height: 0 },
    backgroundColor: "#fff",
    borderTopLeftRadius: 10,
    borderWidth: 1
  },
  card: {
    marginVertical: 8,
    marginHorizontal: 16
  },
  number: {
    width: 40,
    height: 40,
    fontSize: 16,
    borderWidth: 2,
    borderRadius: 40 / 2,
    borderColor: "rgba(46,91,255,0.07)",
    elevation: 1,
    shadowColor: "rgba(46,91,255,0.08)",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: { width: 0, height: 0 },
    alignItems: "center",
    justifyContent: "center"
  },
  insideNumber: {
    fontWeight: "bold"
  },
  question: {
    fontSize: 14,
    marginVertical: 6,
    marginHorizontal: 6
  },
  answerContainer: {
    alignItems: "center",
    justifyContent: "center"
  }
});

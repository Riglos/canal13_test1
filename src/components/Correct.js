import React from "react";
import { Text, StyleSheet } from "react-native";

export default function Correct({ item }) {
  return (
    <Text style={styles.answer}>
      {item.answers.filter(answer => answer.isCorrect)[0].answer}
    </Text>
  );
}

const styles = StyleSheet.create({
  answer: {
    fontSize: 16,
    fontWeight: "bold",
    color: "green"
  }
});

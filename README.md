# Scroll Infinito, paginado cada 10 preguntas.

## Build Setup

```bash
# install dependencies
expo install

# serve with hot reload at localhost:8080
expo start

# build APK for production
expo build:android
```

## Description

Es una aplicación que muestra 10 preguntas al azar junto con su número, pregunta y respuesta correcta.

## Notes

- `react-native-elements` para la interfaz de usuario, `AXIOS` para la llamada a la API y `expo-constants` para manejar el tamaño del statusheader.

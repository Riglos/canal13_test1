import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import Constants from "expo-constants";
import ListScreen from "./src/screens/ListScreen";

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <ListScreen />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
    backgroundColor: "#fff"
  }
});
